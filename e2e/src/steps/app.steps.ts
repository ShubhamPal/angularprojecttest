import { Before, Given, Then, When } from 'cucumber';
import { expect } from 'chai';
import { element, by, Key } from 'protractor';
import { AppPage } from '../pages/app.po';

let page: AppPage;

Before(() => {
  page = new AppPage();
});

Given('I am on the home page', async () => {
  await page.navigateTo();
});

When('I enter {string} as {string}', async (fieldName: string, fieldValue: string) => {
  const fieldId = await element(by.id(fieldName));
  expect(await fieldId.isPresent()).to.be.true;
  await fieldId.sendKeys(fieldValue);
  await fieldId.sendKeys(Key.TAB);
});

When('I click on {string} button', async (buttonName: string) => {
  const buttonId = await element(by.id(buttonName));
  expect(await buttonId.isPresent()).to.be.true;
  await buttonId.click();
});

Then('I should see {string} message as {string}', async (messageClass: string, messageString: string) => {
  const msgNotification = await element(by.css('.' + messageClass)).getText();
  expect(msgNotification).to.be.equal(messageString);
});

Then('I should see {string} as {string}', async function (field: string, messageString: string) {
  const textField = await element(by.id(field));
  expect(await textField.getAttribute('value')).to.be.equal(messageString);
});
