Feature: I can add a student to student list

  As a manager,
  I can add a student to student list
  so that I can populate my student list

  Background:
    Given I am on the home page

  Scenario: I can add a student to student list
    Given I am on the home page
    And I enter "firstName" as "Shubham"
    And I enter "lastName" as "Pal"
    And I enter "emailAddress" as "shubham.pal@nvent.com"
    And I enter "phoneNumber" as "8830128804"
    And I click on "register" button
    When I should see "successMessage" message as "Student added successfully"
    And I am on the "student list" page
    Then I should see "firstName" as "Shubham"
    And I should see "lastName" as "Pal"
    And I should see "emailAddress" as "shubham.pal@nvent.com"
    And I should see "phoneNumber" as "8830128804"

  #validation
  Scenario: I can enter phone number only in numbers
    Given I am on the home page
    When I enter "firstName" as "Shubham"
    And I enter "lastName" as "Pal"
    And I enter "emailAddress" as "shubham.pal@nvent.com"
    And I enter "phoneNumber" as "phoneNumber"
    And I click on "register" button
    Then I should see message as "phone number only contains number"

  Scenario: I cannot enter first name, last name field greater than 50 characters while editing student form
    Given I am on the home page
    When I enter "firstName" as "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    And I enter "lastName" as "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"
    And I enter "emailAddress" as "shubham.pal@nvent.com"
    And I enter "phoneNumber" as "8830128804"
    And I click on "register" button
    Then I should see message as "Please enter 3 to 50 character"
    And I should see message as "Please enter 3 to 50 character"

  Scenario: I cannot enter first name, last name field less than 3 characters while editing student form
    Given I am on the home page
    When I enter "firstName" as "ab"
    And I enter "lastName" as "pa"
    And I enter "emailAddress" as "shubham.pal@nvent.com"
    And I enter "phoneNumber" as "8830128804"
    And I click on "register" button
    Then I should see message as "Please enter 3 to 50 character"
    And I should see message as "Please enter 3 to 50 character"

  Scenario: I can validate email address
    Given I am on the home page
    When I enter "firstName" as "Shubham"
    And I enter "lastName" as "Pal"
    And I enter "emailAddress" as "123234"
    And I enter "phoneNumber" as "8830128804"
    And I click on "register" button
    Then I should see message as "email address should be characters"